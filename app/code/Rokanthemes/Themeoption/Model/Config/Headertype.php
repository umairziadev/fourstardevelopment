<?php
namespace Rokanthemes\Themeoption\Model\Config;

class Headertype implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'header_furniture_01', 'label' => __('Furniture 01')],
            ['value' => 'header_furniture_02', 'label' => __('Furniture 02')],
            ['value' => 'header_furniture_03', 'label' => __('Furniture 03')],
            ['value' => 'header_furniture_04', 'label' => __('Furniture 04')],
            ['value' => 'header_jewelry_01', 'label' => __('Jewelry_01')],
            ['value' => 'header_jewelry_02', 'label' => __('Jewelry_02')],
			['value' => 'header_skincare_01', 'label' => __('Skincare_01')],
            ['value' => 'header_skincare_02', 'label' => __('Skincare_02')], 
			['value' => 'header_plant_01', 'label' => __('Plant_01')],
            ['value' => 'header_plant_02', 'label' => __('Plant_02')],
			['value' => 'header_carparts_01', 'label' => __('Carparts_01')],  
			['value' => 'header_fashion_01', 'label' => __('Fashion_01')],
            ['value' => 'header_fashion_02', 'label' => __('Fashion_02')],  
			['value' => 'header_organic_01', 'label' => __('Organic_01')],
        ];
    }

    public function toArray()
    {
        return [
            'header_furniture_01' => __('Furniture 01'),
            'header_furniture_02' => __('Furniture 02'),
            'header_furniture_03' => __('Furniture 03'),
            'header_furniture_04' => __('Furniture 04'),
            'header_jewelry_01' => __('Jewelry_01'),
            'header_jewelry_02' => __('Jewelry_02'), 
			'header_skincare_01' => __('Skincare_01'),
            'header_skincare_02' => __('Skincare_02'),
			'header_plant_01' => __('Plant_01'),
            'header_plant_02' => __('Plant_02'),
			'header_carparts_01' => __('Carparts_01'),
			'header_fashion_01' => __('Fashion_01'),
            'header_fashion_02' => __('Fashion_02'), 
			'header_organic_01' => __('Organic_01'),
        ];
    }
}